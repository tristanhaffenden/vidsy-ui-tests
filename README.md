# Vidsy UI Automation Tests

## Requirements
* docker
* docker-compose
* yarn (optional - see step 3 on running the tests for more info)

## Running the tests
STEP 1. Clone the repository

```bash
git clone https://bitbucket.org/tristanhaffenden/vidsy-ui-tests.git
```

Then `cd` into the directory of the cloned repo.

STEP 2. Run `docker-compose up` to start the node container for the ui tests and the selenoid container to control the browsers.

```bash
docker-compose up -d
```

STEP 3. Trigger the test run!
If you have `yarn` installed locally, you can just do:

```bash
yarn docker-test
```

This `yarn` command will run the test command inside the running container.
If you don't have `yarn`, you can trigger them manually with the docker:

```bash
docker exec -it vidsy-ui-tests yarn test
``` 

  
NOTE: If you already use `selenoid`, you should make sure it's not currently running before you run the `docker-compose` command.
The compose command creates the network for the containers to talk to each other over, and an existing instance of `selenoid` would not be on that network, so would not be able to talk to the test container.

## Overview
Whilst I could have completed this task in python without a second thought, I decided to tackle it in TypeScript as I wanted to (hopefully) demonstrate not just technical knowledge, but that
I would be a good fit for the role at Vidsy. This is the first proper project I've started using typescript so I'm keen for all feedback and any tooling/library recommendations
I can look into to make my life easier!

I've made some notes in the code and added some TODO statements to mark where I'd improve things with more time.
I didn't get an opportunity to fully complete the briefs test, as I'm unsure of what the expectation for the briefs page is, but I've added comments asking the questions I would need to ask to finish off that test properly.

let yaml = require('js-yaml');
let fs = require('fs');

export function readYamlFile(filePath: string) {
    return yaml.safeLoad(fs.readFileSync(filePath, 'utf8'));
}

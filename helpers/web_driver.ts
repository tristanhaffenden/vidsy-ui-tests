// TODO: define an acceptable load time for user base
const defaultTimeOut = 10000;
const loadingIcon = '.loading';

function waitForLoadingIcon(page, timeout = defaultTimeOut) {
  // Longer timeout than the default as this icon can display for a little while sometimes
  page.waitForElementNotPresent(loadingIcon, timeout);
}

export = {
  defaultTimeOut,
  loadingIcon,
  waitForLoadingIcon
};

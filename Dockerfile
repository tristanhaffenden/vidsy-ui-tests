FROM node:9.10

WORKDIR $HOME/vidsy

COPY . $HOME/vidsy
RUN yarn install
RUN yarn compile

EXPOSE 4444

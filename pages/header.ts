let headerCommands = {
  assertUserNameIs: function(name: string) {
    this.assert.containsText('@userName', name.toUpperCase());
    return this
  },
  assertHeaderTitleIs: function(title: string) {
    this.assert.containsText('@headerTitle', title);
    return this
  },
  clickLogout: function() {
    this.click('@logout');
    this.waitForElementNotPresent('@logout');
    return this
  }
};

export = {
  // No URL value specified as the header is common across pages
  elements: {
    root: 'nav.navigation',
    userName: '.navigation__user-first-name',
    avatar: '.navigation__user-avatar',
    logout: 'nav.navigation button',
    headerTitle: '.navigation__current-info'
  },
  commands: [headerCommands]
};

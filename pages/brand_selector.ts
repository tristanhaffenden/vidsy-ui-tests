const helpers = require('../helpers/web_driver');

let brandSelectorCommands = {
  waitForLoad: function() {
    helpers.waitForLoadingIcon(this);
    this.waitForElementVisible('@root', helpers.defaultTimeOut);
    this.waitForElementVisible('@closeButton', helpers.defaultTimeOut);
    return this
  },
  clickCloseButton: function() {
    this.click('@closeButton');
    this.waitForElementNotPresent('@root', helpers.defaultTimeOut);
    return this
  },
  clickBrandCard: function(brand: string) {
    // TODO: Look at moving this expression to the elements dict
    this.api.useXpath().click(`//*[@class="brand-card__name" and text()="${brand}"]`);
    this.waitForElementNotPresent('@root', helpers.defaultTimeOut);
    return this
  }

};

export = {
  url: function() {
    return this.api.launchUrl + '/dashboard';
  },

  elements: {
    root: '.brand-selector__inner-container',
    closeButton: '.brand-selector__close-button',
    allBrandsSelector: '.container-fluid',
    brandCard: '.brand-card',
    cardName: '.brand-card__name',
  },

  commands: [brandSelectorCommands]
};

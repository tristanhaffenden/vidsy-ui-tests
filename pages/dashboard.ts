const helpers = require('../helpers/web_driver');

let dashboardPageCommands = {
  assertCreateBriefCardIsVisible: function() {
    this.assert.elementPresent('@createBriefCard');
    return this
  },
  assertExistingBriefCardIsVisible: function() {
    // uses wait for present as this element doesn't load straight away
    this.waitForElementPresent('@briefCards', helpers.defaultTimeOut);
    return this
  }
  // TODO: Get count of brief cards to assert correct
  // TODO: Click brief card interactions

};

export = {
  url: function() {
    return this.api.launchUrl + '/dashboard';
  },

  elements: {
    briefCards: '.brief-card',
    createBriefCard: '.create-campaign-card'
  },

  commands: [dashboardPageCommands]
};

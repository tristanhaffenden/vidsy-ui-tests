import { NightwatchBrowser } from 'nightwatch';

const helpers = require('../helpers/web_driver');

let loginPageCommands = {
  enterEmail: function(email: string) {
    this.sendKeys('@emailInput', email);
    return this
  },
  enterPassword: function(password: string) {
    this.sendKeys('@passwordInput', password);
    return this
  },
  clickLogInButton: function() {
    this.click('@logInButton');
    helpers.waitForLoadingIcon(this);
    return this
  },
  waitForLoad: function() {
    this.waitForElementVisible('@emailInput', helpers.defaultTimeOut);
    return this
  },
  loginAsUser: function(browser: NightwatchBrowser, email: string, password: string) {
    let loginPage = browser.page.login();
    loginPage
      .navigate()
      .waitForLoad()
      .enterEmail(email)
      .enterPassword(password)
      .clickLogInButton();
    return this
  }
};


export = {
  url: function() {
    return this.api.launchUrl + '/login';
  },

  elements: {
    emailInput: 'input[name="email"]',
    passwordInput: 'input[name="password"]',
    logInButton: '.login__button',
    forgotPasswordButton: '.login__footer button'
  },

  commands: [loginPageCommands]
};

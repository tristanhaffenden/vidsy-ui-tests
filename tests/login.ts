import { NightwatchBrowser } from 'nightwatch';
import { readYamlFile } from '../helpers/file_io'

let credentials = readYamlFile('config/credentials.yaml');

export = {
  'Login with valid credentials': (client: NightwatchBrowser) => {
    const env = client.globals.env;
    let loginPage = client.page.login();
    loginPage
      .navigate()
      .waitForLoad()
      .enterEmail(credentials[env]['email'])
      .enterPassword(credentials[env]['password'])
      .clickLogInButton();

    // Verify the url is correct after logging in
    let brandSelector = client.page.brand_selector();
    brandSelector.waitForLoad();
    client.assert.urlEquals(brandSelector.url());

    // Verify the user name shows in the header
    let header = client.page.header();
    header.assertUserNameIs(credentials[env]['name']);

    client.end();
  }
};
// TODO: Add test for invalid credentials
// TODO: Add tests for empty fields

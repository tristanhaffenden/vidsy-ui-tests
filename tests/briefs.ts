import { NightwatchBrowser } from 'nightwatch';
import { readYamlFile } from '../helpers/file_io'

let credentials = readYamlFile('config/credentials.yaml');

export = {
  before: function(client: NightwatchBrowser) {
    // Login before the test runs
    let loginPage = client.page.login();
    const env = client.globals.env;
    loginPage.loginAsUser(client, credentials[env]['email'], credentials[env]['password']);
  },

  'List of Briefs': (client: NightwatchBrowser) => {
    const brand = 'Listerine';
    let brandSelector = client.page.brand_selector();
    brandSelector
      .waitForLoad()
      .clickBrandCard(brand);

    // Verify the brand has been selected
    let header = client.page.header();
    header.assertHeaderTitleIs(brand);

    let dashboard = client.page.dashboard();
    dashboard
      .assertCreateBriefCardIsVisible()
      .assertExistingBriefCardIsVisible();
    // TODO: confirm what a usual briefs view would be
    // Are they always displayed?
    // Are old ones archived or always visible on this page?
    // Do all users have the create brief permission?
    // If there is more than one how should they be ordered?

    client.end()
  }
};